### **Сервис комментариев**

Является частью децентрализованного [**приложения**](https://github.com/rtemka/agg)

#### Эндпоинты

```bash
# запрос
curl -X GET http://[host:port]/comments

# ответ 
# [{"id": 1,"author": "alice", "text": "test comment", "posted_at": 1659955818},... ] ... 200 OK
```
```bash
# запрос
curl -X POST -d '{"news_id":1, "text":"test comment example", "posted_at":1661262350, "author_id":1}' http://[host:port]/comments

# ответ 
# {"id": 1} ... 201 Created
```